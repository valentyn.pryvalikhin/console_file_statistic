package com.valikbr.service;

import com.valikbr.entity.FileStatistic;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.Assert.*;

public class FileServiceImplTest {

    File file;
    FileService fileService = new FileServiceImpl();
    FileStatistic fileStatistic;

    @Before
    public void setUp() {
        try {
            file = tempFolder.newFile("test.txt");
            FileWriter writer = new FileWriter(file.getAbsolutePath(), true);
            BufferedWriter bufferWriter = new BufferedWriter(writer);
            bufferWriter.write("first last one\n");
            bufferWriter.write("\n");
            bufferWriter.write("tree jump\n");
            bufferWriter.write("dead\n");
            bufferWriter.write("good, bad");
            bufferWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        fileStatistic = new FileStatistic();
    }

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Test
    public void test_calculateFileStatistic_name() {
        //GIVEN
        String expected = file.getAbsolutePath();

        //WHEN
        try {
           fileStatistic = fileService.calculateFileStatistic(file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        String result = fileStatistic.getFileName();
        //THEN
        Assert.assertEquals(expected, result);
    }

    @Test
    public void test_calculateFileStatistic_longestWord() {
        //GIVEN
        int expected = 5;

        //WHEN
        try {
           fileStatistic = fileService.calculateFileStatistic(file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        int result = fileStatistic.getLongestWord();
        //THEN
        Assert.assertEquals(expected, result);
    }

    @Test
    public void test_calculateFileStatistic_shortestWord() {
        //GIVEN
        int expected = 0;

        //WHEN
        try {
           fileStatistic = fileService.calculateFileStatistic(file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        int result = fileStatistic.getShortestWord();
        //THEN
        Assert.assertEquals(expected, result);
    }

    @Test
    public void test_calculateFileStatistic_averageLineLength() {
        //GIVEN
        double expected = 7;

        //WHEN
        try {
           fileStatistic = fileService.calculateFileStatistic(file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        double result = fileStatistic.getAverageLineLenght();
        //THEN
        Assert.assertEquals(expected, result, 0.01);
    }

    @Test
    public void test_calculateFileStatistic_averageWordLenth() {
        //GIVEN
        double expected = 3.2;

        //WHEN
        try {
           fileStatistic = fileService.calculateFileStatistic(file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        double result = fileStatistic.getAverageWordLength();
        //THEN
        Assert.assertEquals(expected, result, 0.01);
    }
}
