package com.valikbr.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LineServiceImplTest {

    LineService lineService;
    String line;
    String emptyLine;

    @Before
    public void setUp(){
        //GIVEN
        lineService = new LineServiceImpl();
        line = "Now we are ready to test the changes by applying them to the database";
        emptyLine = "";
    }


    @Test
    public void test_calculateLineLongestWord() {
        //GIVEN
        int expected = 8;
        //WHEN
        int result = lineService.calculateLineLongestWord(line);
        //THEN
        Assert.assertEquals(expected, result);
    }

     @Test
    public void test_calculateLineLongestWord_when_line_is_empty() {
        //GIVEN
        int expected = 0;
        //WHEN
        int result = lineService.calculateLineLongestWord(emptyLine);
        //THEN
        Assert.assertEquals(expected, result);
    }

    @Test
    public void test_calculateLineShortestWord() {
        //GIVEN
        int expected = 2;
        //WHEN
        int result = lineService.calculateLineShortestWord(line);
        //THEN
        Assert.assertEquals(expected, result);
    }

    @Test
    public void test_calculateLineShortestWord_when_line_is_empty() {
        //GIVEN
        int expected = 0;
        //WHEN
        int result = lineService.calculateLineShortestWord(emptyLine);
        //THEN
        Assert.assertEquals(expected, result);
    }

    @Test
    public void test_calculateLineLenght() {
            //GIVEN
            long expected = 69;
            //WHEN
            long result = lineService.calculateLineLenght(line);
            //THEN
            Assert.assertEquals(expected, result);
    }

 @Test
    public void test_calculateLineLenght_when_line_is_empty() {
            //GIVEN
            long expected = 0;
            //WHEN
            long result = lineService.calculateLineLenght(emptyLine);
            //THEN
            Assert.assertEquals(expected, result);
    }

    @Test
    public void test_calculateLineAvarageWordLength() {
        //GIVEN
        double expected = 4.0;
        //WHEN
        double result = lineService.calculateLineAvarageWordLength(line);
        //THEN
        Assert.assertEquals(expected, result, 0.01);
    }

    @Test
    public void test_calculateLineAvarageWordLength_when_line_is_empty() {
        //GIVEN
        double expected = 0;
        //WHEN
        double result = lineService.calculateLineAvarageWordLength(emptyLine);
        //THEN
        Assert.assertEquals(expected, result, 0.01);
    }
}
