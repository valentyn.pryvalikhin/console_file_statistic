package com.valikbr.service;

public interface LineService {
    public int calculateLineLongestWord(String line);

    public int calculateLineShortestWord(String line);

    public long calculateLineLenght(String line);

    public double calculateLineAvarageWordLength(String line);

}
