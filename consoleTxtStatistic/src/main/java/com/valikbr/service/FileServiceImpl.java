package com.valikbr.service;

import com.valikbr.entity.FileStatistic;
import com.valikbr.entity.LineStatistic;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileServiceImpl implements FileService {
    @Override
    public FileStatistic calculateFileStatistic(String fileName) throws IOException {

        File fileForReading = new File(fileName);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileForReading)));
        } catch (FileNotFoundException e) {
            System.out.println("Can't open file " + fileName);
            e.printStackTrace();
        }

        LineService lineService = new LineServiceImpl();
        FileStatistic fileStatistic = new FileStatistic();

        fileStatistic.setFileName(fileName);

        String line;

        int fileLongestWord = 0;
        Integer fileShortestWord = null;
        double fileAverageLineLenght = 0;
        long fileTotalLineLenght = 0;
        double fileAverageWordLength = 0;
        double fileTotalAverageWordLength = 0;
        List<LineStatistic> lineStatistics = new ArrayList<>();

        int lineNumber = 0;

        while ((line = reader.readLine()) != null) {

            int lineLongestWord = 0;
            int lineShortestWord = 0;
            long lineLenght = 0;
            double lineAverageWordLength = 0;

            LineStatistic lineStatistic = new LineStatistic();

            lineLenght = lineService.calculateLineLenght(line);
            lineLongestWord = lineService.calculateLineLongestWord(line);
            lineShortestWord = lineService.calculateLineShortestWord(line);
            lineAverageWordLength = lineService.calculateLineAvarageWordLength(line);

            lineStatistic.setLongestWord(lineLongestWord);
            lineStatistic.setShortestWord(lineShortestWord);
            lineStatistic.setNumber(lineNumber);
            lineStatistic.setAverageWordLength(lineAverageWordLength);
            lineStatistic.setLineLenght(lineLenght);

            lineNumber++;

            if (lineLongestWord > fileLongestWord) fileLongestWord = lineLongestWord;
            if (fileShortestWord == null) fileShortestWord = lineShortestWord;
            if (lineShortestWord < fileShortestWord) fileShortestWord = lineShortestWord;
            fileTotalLineLenght = fileTotalLineLenght + lineLenght;
            fileTotalAverageWordLength = fileTotalAverageWordLength + lineAverageWordLength;

            lineStatistics.add(lineStatistic);
        }

        int fileNumberOfLines = lineStatistics.size();
        fileAverageLineLenght = fileTotalLineLenght / fileNumberOfLines;
        fileAverageWordLength = fileTotalAverageWordLength / fileNumberOfLines;

        fileStatistic.setLongestWord(fileLongestWord);
        fileStatistic.setShortestWord(fileShortestWord);
        fileStatistic.setAverageLineLenght(fileAverageLineLenght);
        fileStatistic.setAverageWordLength(fileAverageWordLength);
        fileStatistic.setLineStatistics(lineStatistics);

        reader.close();

        return fileStatistic;
    }
}
