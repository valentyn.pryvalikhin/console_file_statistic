package com.valikbr.service;

import com.valikbr.entity.FileStatistic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public interface FileService {
    public FileStatistic calculateFileStatistic(String fileName) throws IOException;
}
