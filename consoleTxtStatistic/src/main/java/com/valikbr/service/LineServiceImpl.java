package com.valikbr.service;

public class LineServiceImpl implements LineService {
    @Override
    public int calculateLineLongestWord(String line) {
        String[] wordList = line.split("\\s+");

        int longestWordLenght = 0;

        for (String word : wordList) {
            if (word.length() > longestWordLenght)
                longestWordLenght = word.length();
        }
        return longestWordLenght;
    }

    @Override
    public int calculateLineShortestWord(String line) {
        String[] wordList = line.split("\\s+");

        int shortestWordLenght = wordList[0].length();

        for (String word : wordList) {
            if (word.length() < shortestWordLenght)
                shortestWordLenght = word.length();
        }
        return shortestWordLenght;
    }

    @Override
    public long calculateLineLenght(String line) {
        return line.length();
    }

    @Override
    public double calculateLineAvarageWordLength(String line) {
        String[] wordList = line.split("\\s+");

        double avarageWordLenght = 0;
        int totalWordsLength = 0;

        for (String word : wordList) totalWordsLength = totalWordsLength + word.length();

        return avarageWordLenght = totalWordsLength / wordList.length;
    }
}
