package com.valikbr;

import com.valikbr.dao.FileStatisticDao;
import com.valikbr.dao.FileStatisticDaoImpl;
import com.valikbr.dao.exception.DaoSystemException;
import com.valikbr.entity.FileStatistic;
import com.valikbr.service.FileService;
import com.valikbr.service.FileServiceImpl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class App {

    public static void main(String[] args) {
        FileService fileService = new FileServiceImpl();
        FileStatisticDao fileStatisticDao = new FileStatisticDaoImpl();

        try {
            FileStatistic fileStatistic = fileService.calculateFileStatistic(args[0]);
            try {
                fileStatisticDao.insert(fileStatistic);
            } catch (DaoSystemException e) {
                System.out.println("Can't add file statistic to DB");
                e.printStackTrace();
            }
        } catch (IOException e) {
            System.out.println("Can't read file " + args[0]);
            e.printStackTrace();
        }
    }
}
