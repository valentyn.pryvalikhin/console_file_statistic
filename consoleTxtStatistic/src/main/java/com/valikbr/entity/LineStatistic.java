package com.valikbr.entity;

import java.io.Serializable;
import java.util.Objects;

public class LineStatistic implements Serializable {

    private static final long serialVersionUID = -507881162403181710L;

    private long id;
    private int number;
    private Integer longestWord;
    private Integer shortestWord;
    private long lineLenght;
    private double averageWordLength;
    private long fileId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getLongestWord() {
        return longestWord;
    }

    public void setLongestWord(int longestWord) {
        this.longestWord = longestWord;
    }

    public int getShortestWord() {
        return shortestWord;
    }

    public void setShortestWord(int shortestWord) {
        this.shortestWord = shortestWord;
    }

    public long getLineLenght() {
        return lineLenght;
    }

    public void setLineLenght(long lineLenght) {
        this.lineLenght = lineLenght;
    }

    public double getAverageWordLength() {
        return averageWordLength;
    }

    public void setAverageWordLength(double averageWordLength) {
        this.averageWordLength = averageWordLength;
    }

    public long getFileId() {
        return fileId;
    }

    public void setFileId(long fileId) {
        this.fileId = fileId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LineStatistic that = (LineStatistic) o;
        return id == that.id &&
                number == that.number &&
                longestWord == that.longestWord &&
                shortestWord == that.shortestWord &&
                lineLenght == that.lineLenght &&
                Double.compare(that.averageWordLength, averageWordLength) == 0 &&
                fileId == that.fileId;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, number, longestWord, shortestWord, lineLenght, averageWordLength, fileId);
    }

    @Override
    public String toString() {
        return "LineStatistic{" +
                "id=" + id +
                ", number=" + number +
                ", longestWord=" + longestWord +
                ", shortestWord=" + shortestWord +
                ", lineLenght=" + lineLenght +
                ", averageWordLength=" + averageWordLength +
                ", fileId=" + fileId +
                '}';
    }
}
