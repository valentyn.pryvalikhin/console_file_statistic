package com.valikbr.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FileStatistic implements Serializable {

    private static final long serialVersionUID = 3248070021776260356L;

    private long id;
    private String fileName;
    private int longestWord;
    private int shortestWord;
    private double averageLineLenght;
    private double averageWordLength;
    private List<LineStatistic> lineStatistics = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getLongestWord() {
        return longestWord;
    }

    public void setLongestWord(int longestWord) {
        this.longestWord = longestWord;
    }

    public int getShortestWord() {
        return shortestWord;
    }

    public void setShortestWord(int shortestWord) {
        this.shortestWord = shortestWord;
    }

    public double getAverageLineLenght() {
        return averageLineLenght;
    }

    public void setAverageLineLenght(double averageLineLenght) {
        this.averageLineLenght = averageLineLenght;
    }

    public double getAverageWordLength() {
        return averageWordLength;
    }

    public void setAverageWordLength(double averageWordLength) {
        this.averageWordLength = averageWordLength;
    }

    public List<LineStatistic> getLineStatistics() {
        return lineStatistics;
    }

    public void setLineStatistics(List<LineStatistic> lineStatistics) {
        this.lineStatistics = lineStatistics;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileStatistic that = (FileStatistic) o;
        return id == that.id &&
                longestWord == that.longestWord &&
                shortestWord == that.shortestWord &&
                Double.compare(that.averageLineLenght, averageLineLenght) == 0 &&
                Double.compare(that.averageWordLength, averageWordLength) == 0 &&
                Objects.equals(fileName, that.fileName) &&
                Objects.equals(lineStatistics, that.lineStatistics);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, fileName, longestWord, shortestWord, averageLineLenght, averageWordLength, lineStatistics);
    }

    @Override
    public String toString() {
        return "FileStatistic{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                ", longestWord=" + longestWord +
                ", shortestWord=" + shortestWord +
                ", averageLineLenght=" + averageLineLenght +
                ", averageWordLength=" + averageWordLength +
                ", lineStatistics=" + lineStatistics +
                '}';
    }
}
