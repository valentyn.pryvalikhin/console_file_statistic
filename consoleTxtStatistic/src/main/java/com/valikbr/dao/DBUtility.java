package com.valikbr.dao;

import com.valikbr.dao.exception.DaoSystemException;
import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBUtility {

    public static String DB_USER_NAME = "root";
    public static String DB_PASSWORD = "root";
    public static String DB_URL = "jdbc:mariadb://localhost:3306/txt_statistic?useUnicode=true&amp;characterEncoding=utf8";
    public static String DB_DRIVER = "org.mariadb.jdbc.Driver";

    private static BasicDataSource dataSource;

    public static Connection getConnection() throws DaoSystemException {
        try {
            Connection connection = getDataSource().getConnection();
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            connection.setAutoCommit(false);
            return connection;
        } catch (SQLException e) {
            throw new DaoSystemException("Can't get connection from JdbcConnectionPool", e);
        }
    }

    private static BasicDataSource getDataSource() {

        if (dataSource == null) {
            BasicDataSource ds = new BasicDataSource();
            ds.setUrl(DB_URL);
            ds.setUsername(DB_USER_NAME);
            ds.setPassword(DB_PASSWORD);
            ds.setDriverClassName(DB_DRIVER);

            ds.setMinIdle(5);
            ds.setMaxIdle(10);
            ds.setMaxOpenPreparedStatements(100);

            dataSource = ds;
        }
        return dataSource;
    }

    public static void closeQuietly(Connection conn) {
        try {
            conn.close();
        } catch (Exception e) {
        }
    }

    public static void closeQuietly(PreparedStatement preparedStatement) {
        try {
            preparedStatement.close();
        } catch (Exception e) {
        }
    }

    public static void rollbackQuietly(Connection conn) {
        try {
            conn.rollback();
        } catch (Exception e) {
        }
    }
}
