package com.valikbr.dao;

import com.valikbr.dao.exception.DaoSystemException;
import com.valikbr.entity.FileStatistic;
import com.valikbr.entity.LineStatistic;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FileStatisticDaoImpl implements FileStatisticDao {

    LineStatisticDao lineStatisticDao = new LineStatisticDaoImpl();

    @Override
    public FileStatistic selectById(int id) throws DaoSystemException {
        String sql = "SELECT * FROM file_statistic WHERE id = " + id;
        Connection connection = DBUtility.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            resultSet.next();
            FileStatistic fileStatistic = proccesFileStatistic(resultSet);
            connection.commit();

            return fileStatistic;

        } catch (SQLException e) {
            DBUtility.rollbackQuietly(connection);
            throw new DaoSystemException("Can't execute query:" + sql, e);
        } finally {
            DBUtility.closeQuietly(preparedStatement);
            DBUtility.closeQuietly(connection);
        }
    }

    @Override
    public FileStatistic selectLastEntry() throws DaoSystemException {
        String sql = "SELECT * FROM file_statistic ORDER BY id DESC LIMIT 1";
        Connection connection = DBUtility.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            resultSet.next();
            FileStatistic fileStatistic = proccesFileStatistic(resultSet);
            connection.commit();

            return fileStatistic;

        } catch (SQLException e) {
            DBUtility.rollbackQuietly(connection);
            throw new DaoSystemException("Can't execute query:" + sql, e);
        } finally {
            DBUtility.closeQuietly(preparedStatement);
            DBUtility.closeQuietly(connection);
        }
    }

    @Override
    public List<FileStatistic> selectAll() throws DaoSystemException {
        String sql = "SELECT * FROM file_statistic";
        List<FileStatistic> fileStatistics = new ArrayList<>();
        Connection connection = DBUtility.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            fileStatistics = proccesFileStatistics(resultSet);
            connection.commit();
            return fileStatistics;

        } catch (SQLException e) {
            DBUtility.rollbackQuietly(connection);
            throw new DaoSystemException("Can't execute query:" + sql, e);
        } finally {
            DBUtility.closeQuietly(preparedStatement);
            DBUtility.closeQuietly(connection);
        }
    }

    @Override
    public void insert(FileStatistic newEntity) throws DaoSystemException {
        String sql = "INSERT INTO file_statistic " +
                "(file_name, longest_word, shortest_word, avarage_line_length, avarage_word_length) VALUES (?, ?, ?, ?, ?)";
        Connection connection = DBUtility.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, newEntity.getFileName());
            preparedStatement.setInt(2, newEntity.getLongestWord());
            preparedStatement.setInt(3, newEntity.getShortestWord());
            preparedStatement.setDouble(4, newEntity.getAverageLineLenght());
            preparedStatement.setDouble(5, newEntity.getAverageWordLength());
            preparedStatement.executeUpdate();

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();

            if (generatedKeys.next()) {
                for (LineStatistic lineStatistic : newEntity.getLineStatistics()) {
                    lineStatistic.setFileId(generatedKeys.getLong(1));
                }
            }

            connection.commit();

            List<LineStatistic> lineStatistics = newEntity.getLineStatistics();
            for (LineStatistic lineStatistic : lineStatistics) {
                lineStatisticDao.insert(lineStatistic);
            }

        } catch (SQLException e) {
            DBUtility.rollbackQuietly(connection);
            throw new DaoSystemException("Can't execute query:" + sql, e);
        } finally {
            DBUtility.closeQuietly(preparedStatement);
            DBUtility.closeQuietly(connection);
        }
    }

    @Override
    public void update(FileStatistic newEntity) throws DaoSystemException {
        String sql = "UPDATE file_statistic SET file_name = ?, longest_word = ?, shortest_word = ?," +
                " avarage_line_length = ?, avarage_word_length = ? WHERE id = ?";
        Connection connection = DBUtility.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, newEntity.getFileName());
            preparedStatement.setInt(2, newEntity.getLongestWord());
            preparedStatement.setInt(3, newEntity.getShortestWord());
            preparedStatement.setDouble(4, newEntity.getAverageLineLenght());
            preparedStatement.setDouble(5, newEntity.getAverageWordLength());
            preparedStatement.setLong(6, newEntity.getId());
            preparedStatement.executeUpdate();

            connection.commit();

            for (LineStatistic lineStatistic : newEntity.getLineStatistics()) {
                lineStatisticDao.insert(lineStatistic);
            }


        } catch (SQLException e) {
            DBUtility.rollbackQuietly(connection);
            throw new DaoSystemException("Can't execute query:" + sql, e);
        } finally {
            DBUtility.closeQuietly(preparedStatement);
            DBUtility.closeQuietly(connection);
        }
    }

    @Override
    public void deleteById(int id) throws DaoSystemException {
        String sql = "DELETE FROM file_statistic WHERE id = ?";
        Connection connection = DBUtility.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.executeUpdate();

            connection.commit();

        } catch (SQLException e) {
            DBUtility.rollbackQuietly(connection);
            throw new DaoSystemException("Can't execute query:" + sql, e);
        } finally {
            DBUtility.closeQuietly(preparedStatement);
            DBUtility.closeQuietly(connection);
        }
    }

    private FileStatistic proccesFileStatistic(ResultSet resultSet) throws SQLException {

        FileStatistic fileStatistic = new FileStatistic();
        fileStatistic.setId(resultSet.getLong("id"));
        fileStatistic.setFileName(resultSet.getString("file_name"));
        fileStatistic.setLongestWord(resultSet.getInt("longest_word"));
        fileStatistic.setShortestWord(resultSet.getInt("shortest_word"));
        fileStatistic.setAverageLineLenght(resultSet.getDouble("avarage_line_length"));
        fileStatistic.setAverageWordLength(resultSet.getDouble("avarage_word_length"));

        List<LineStatistic> lineStatistics = new ArrayList<>();
        try {
            lineStatistics = lineStatisticDao.selectByFileId(resultSet.getLong("id"));
        } catch (DaoSystemException e) {
            e.printStackTrace();
        }
        fileStatistic.setLineStatistics(lineStatistics);

        return fileStatistic;
    }

    private List<FileStatistic> proccesFileStatistics(ResultSet resultSet) throws SQLException {
        List<FileStatistic> fileStatistics = new ArrayList<>();
        while (resultSet.next()) {
            fileStatistics.add(proccesFileStatistic(resultSet));
        }
        return fileStatistics;
    }
}
