package com.valikbr.dao;

import com.valikbr.dao.exception.DaoSystemException;
import com.valikbr.entity.LineStatistic;

import java.util.List;

public interface LineStatisticDao extends Dao<LineStatistic> {

    public List<LineStatistic> selectByFileId(long id) throws DaoSystemException;
}
