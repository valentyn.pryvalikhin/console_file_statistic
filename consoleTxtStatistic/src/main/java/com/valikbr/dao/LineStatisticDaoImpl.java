package com.valikbr.dao;

import com.valikbr.dao.exception.DaoSystemException;
import com.valikbr.entity.LineStatistic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LineStatisticDaoImpl implements LineStatisticDao {

    @Override
    public LineStatistic selectById(int id) throws DaoSystemException {
        String sql = "SELECT * FROM line_statistic WHERE id = " + id;
        Connection connection = DBUtility.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            resultSet.next();
            LineStatistic lineStatistic = proccesLineStatistic(resultSet);
            connection.commit();

            return lineStatistic;

        } catch (SQLException e) {
            DBUtility.rollbackQuietly(connection);
            throw new DaoSystemException("Can't execute query:" + sql, e);
        } finally {
            DBUtility.closeQuietly(preparedStatement);
            DBUtility.closeQuietly(connection);
        }
    }

    @Override
    public List<LineStatistic> selectAll() throws DaoSystemException {
        String sql = "SELECT * FROM line_statistic";
        List<LineStatistic> lineStatistics = new ArrayList<>();
        Connection connection = DBUtility.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            lineStatistics = proccesLineStatistics(resultSet);
            connection.commit();
            return lineStatistics;

        } catch (SQLException e) {
            DBUtility.rollbackQuietly(connection);
            throw new DaoSystemException("Can't execute query:" + sql, e);
        } finally {
            DBUtility.closeQuietly(preparedStatement);
            DBUtility.closeQuietly(connection);
        }
    }

    @Override
    public List<LineStatistic> selectByFileId(long id) throws DaoSystemException {
        String sql = "SELECT * FROM line_statistic WHERE id =" + id;
        List<LineStatistic> lineStatistics = new ArrayList<>();
        Connection connection = DBUtility.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            lineStatistics = proccesLineStatistics(resultSet);
            connection.commit();
            return lineStatistics;

        } catch (SQLException e) {
            DBUtility.rollbackQuietly(connection);
            throw new DaoSystemException("Can't execute query:" + sql, e);
        } finally {
            DBUtility.closeQuietly(preparedStatement);
            DBUtility.closeQuietly(connection);
        }
    }

    @Override
    public void insert(LineStatistic newEntity) throws DaoSystemException {
        String sql = "INSERT INTO line_statistic " +
                "(number, longest_word, shortest_word, avarage_word_length, line_lenght, file_id) VALUES (?, ?, ?, ?, ?, ?)";
        Connection connection = DBUtility.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, newEntity.getNumber());
            preparedStatement.setInt(2, newEntity.getLongestWord());
            preparedStatement.setInt(3, newEntity.getShortestWord());
            preparedStatement.setDouble(4, newEntity.getAverageWordLength());
            preparedStatement.setLong(5, newEntity.getLineLenght());
            preparedStatement.setLong(6, newEntity.getFileId());

            preparedStatement.executeUpdate();

            connection.commit();

        } catch (SQLException e) {
            DBUtility.rollbackQuietly(connection);
            throw new DaoSystemException("Can't execute query:" + sql, e);
        } finally {
            DBUtility.closeQuietly(preparedStatement);
            DBUtility.closeQuietly(connection);
        }
    }

    @Override
    public void update(LineStatistic newEntity) throws DaoSystemException {
        String sql = "UPDATE line_statistic SET number = ?, longest_word = ?, shortest_word = ?," +
                " avarage_word_length = ?, line_lenght = ?, file_id = ? WHERE id = ?";
        Connection connection = DBUtility.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, newEntity.getNumber());
            preparedStatement.setInt(2, newEntity.getLongestWord());
            preparedStatement.setInt(3, newEntity.getShortestWord());
            preparedStatement.setDouble(4, newEntity.getAverageWordLength());
            preparedStatement.setLong(5, newEntity.getLineLenght());
            preparedStatement.setLong(6, newEntity.getFileId());
            preparedStatement.setLong(7, newEntity.getId());

            preparedStatement.executeUpdate();

            connection.commit();

        } catch (SQLException e) {
            DBUtility.rollbackQuietly(connection);
            throw new DaoSystemException("Can't execute query:" + sql, e);
        } finally {
            DBUtility.closeQuietly(preparedStatement);
            DBUtility.closeQuietly(connection);
        }
    }

    @Override
    public void deleteById(int id) throws DaoSystemException {
        String sql = "DELETE FROM line_statistic WHERE id = ?";
        Connection connection = DBUtility.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.executeUpdate();

            connection.commit();

        } catch (SQLException e) {
            DBUtility.rollbackQuietly(connection);
            throw new DaoSystemException("Can't execute query:" + sql, e);
        } finally {
            DBUtility.closeQuietly(preparedStatement);
            DBUtility.closeQuietly(connection);
        }
    }

    private LineStatistic proccesLineStatistic(ResultSet resultSet) throws SQLException {


        LineStatistic lineStatistic = new LineStatistic();
        lineStatistic.setId(resultSet.getInt("id"));
        lineStatistic.setNumber(resultSet.getInt("number"));
        lineStatistic.setLongestWord(resultSet.getInt("longest_word"));
        lineStatistic.setShortestWord(resultSet.getInt("shortest_word"));
        lineStatistic.setAverageWordLength(resultSet.getDouble("avarage_word_length"));
        lineStatistic.setLineLenght(resultSet.getLong("line_lenght"));
        lineStatistic.setFileId(resultSet.getLong("file_id"));

        return lineStatistic;
    }

    private List<LineStatistic> proccesLineStatistics(ResultSet resultSet) throws SQLException {
        List<LineStatistic> lineStatistics = new ArrayList<>();
        while (resultSet.next()) {
            lineStatistics.add(proccesLineStatistic(resultSet));
        }
        return lineStatistics;
    }
}
