package com.valikbr.dao.exception;

public class DaoSystemException extends Exception {
    public DaoSystemException(String message) {
        super(message);
    }

    public DaoSystemException(String massege, Throwable cause) {
        super(massege, cause);
    }
}
