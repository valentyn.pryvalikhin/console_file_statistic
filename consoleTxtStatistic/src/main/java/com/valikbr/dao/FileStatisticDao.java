package com.valikbr.dao;

import com.valikbr.dao.exception.DaoSystemException;
import com.valikbr.entity.FileStatistic;

public interface FileStatisticDao extends Dao<FileStatistic> {
    public FileStatistic selectLastEntry() throws DaoSystemException;
}
