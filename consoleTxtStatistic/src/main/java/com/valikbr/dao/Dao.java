package com.valikbr.dao;

import com.valikbr.dao.exception.DaoSystemException;

import java.util.List;

public interface Dao<T> {
    public T selectById(int id) throws DaoSystemException;

    public List<T> selectAll() throws DaoSystemException;

    public void insert(T newEntity) throws DaoSystemException;

    public void update(T newEntity) throws DaoSystemException;

    public void deleteById(int id) throws DaoSystemException;
}
